package info.hccis.camper.bo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Class to test CamperBO methods
 * @author cis2232
 * @since 20191210
 */
public class CamperBOJUnitTest {
    
    CamperBO camperTestObj;
    public CamperBOJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void test_getCost() {
        camperTestObj = new CamperBO();
        double result = camperTestObj.getCost(1);
        assertEquals(150, result,150.00);
        
    }
     @Test
    public void test_getCost2() {
        camperTestObj = new CamperBO();
        double result = camperTestObj.getCost(2);
        assertEquals(100, result,100.00);
        
    }
     @Test
    public void test_getCost3() {
        camperTestObj = new CamperBO();
        double result = camperTestObj.getCost(3);
        assertEquals(150, result,150.00);
        
    }
}
