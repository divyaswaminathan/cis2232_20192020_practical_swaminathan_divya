package info.hccis.camper.bo;

import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.jpa.Camper;

/**
 * Contains camper model functionality.
 * @author bjmaclean
 * Edited by: Divya Swaminathan
 * @since Nov 6, 2019
 */
public class CamperBO {

    CamperRepository cr = null;
    // declaring constants for cost
    public final double SUMMER_CAMP_COST =100;
    public final double OTHER_CAMP_COST =150;
    
    private static double cost;

    public CamperBO() {
        
    }
    

    public CamperRepository getCr() {
        return cr;
    }

    public void setCr(CamperRepository cr) {
        this.cr = cr;
    }

  

    public void setCost(double cost) {
       
        this.cost = cost;
    }
    
    public CamperBO(CamperRepository cr) {
        this.cr = cr;
    }

    public Camper getCamper(int id){
        return cr.findOne(id);
    }

    /**
     * This method will return the cost of a camp.
     * @since 20191210
     * @author cis1232
     */
    public double getCost(int campType){
        switch(campType)
        {
            case 1:
            case 3:
                setCost(OTHER_CAMP_COST); 
                break;
            case 2:
                 setCost(SUMMER_CAMP_COST);
                break;
            default:
                break;
        }
        return this.cost;
    }

}
