package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.entity.CamperEntity;
import info.hccis.camper.jpa.Camper;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Services related to campers
 *
 * @author bjmaclean
 * @since Nov 6, 2019
 */
@Path("/CamperService")
public class CamperRest {

    @Resource
    private final CamperRepository cr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * CamperRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM
     */
    public CamperRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cr = applicationContext.getBean(CamperRepository.class);
    }

    @GET
    //@Produces({MediaType.APPLICATION_JSON}) 
    @Path("camper/{param}")
    public Response getCamper(@PathParam("param") String camperId) {

        String camperJson = "test" + camperId;

        //Camper camper = CamperDAO.select(Integer.parseInt(camperId));
        Camper camper = cr.findOne(Integer.parseInt(camperId));

        if (camper == null) {
            return Response.status(204).entity("{}").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(camper)).build();
        }

    }
    /*
    show campers based on camper type
    */
    @GET
    //@Produces({MediaType.APPLICATION_JSON}) 
    @Path("camper/campType/{param1}")
    public Response getCamperType(@PathParam("param1") String camperType) {

        String camperJson = "test" + camperType;

        //Camper camper = CamperDAO.select(Integer.parseInt(camperId));
        ArrayList<CamperEntity> camper = CamperDAO.selectAllByCamperType(Integer.parseInt(camperType));
       // Camper camper = cr.findOne(Integer.parseInt(camperType));

        if (camper == null) {
            return Response.status(204).entity("{}").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(camper)).build();
        }

    }

    /**
     * Service to provide all campers
     *
     * @author bjmaclean
     * @since Nov 6, 2019
     */
    @GET
    //@Produces({MediaType.APPLICATION_JSON}) 
    @Path("/camper")
    public Response getCampers() {

        //ArrayList<info.hccis.camper.entity.Camper> campers = CamperDAO.selectAll();
        ArrayList<Camper> campers = (ArrayList<Camper>) cr.findAll();

        if (campers == null) {
            return Response.status(204).entity("[]").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(campers)).build();
        }

    }

    /**
     * POST operation which will update a camper.
     *
     * @param jsonIn
     * @return response including the json representing the new camper.
     * @throws IOException
     */
    @POST
    @Path("/camper")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCamper(String jsonIn) throws IOException {

//        ObjectMapper mapper = new ObjectMapper();
        //JSON from String to Object
        Gson gson = new Gson();
        info.hccis.camper.jpa.Camper camper = gson.fromJson(jsonIn, info.hccis.camper.jpa.Camper.class);
        //Camper camper = mapper.readValue(jsonIn, Camper.class);
        camper = cr.save(camper);
        String temp = "";
        temp = gson.toJson(camper);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

}
