package info.hccis.camper.web;

import info.hccis.camper.bo.CamperBO;
import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.dao.CodeValueDAO;
import info.hccis.camper.dao.DatabaseConnection;
import info.hccis.camper.data.springdatajpa.CamperRepository;
//import info.hccis.camper.jpa.Camper;
import info.hccis.camper.entity.CamperEntity;
import info.hccis.camper.jpa.UserAccess;
import info.hccis.util.Utility;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CamperController {

    private final CamperRepository cr;

    /**
     * This constructor will inject the Repository object into this controller.
     * It will allow this class' methods to use methods from this Spring JPA
     * repository object.
     *
     * @since 20191021
     * @author BJM
     * @param ur
     */
    @Autowired
    public CamperController(CamperRepository cr) {
        this.cr = cr;
    }

    @RequestMapping("/camper/add")
    public String add(Model model, HttpSession session) {
        boolean loggedIn = Utility.verifyLogin(session);
        //Verify that the user is logged in
        if (!loggedIn) {
            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
            model.addAttribute("user", new UserAccess());
            return "other/welcome";
        }

//        //Get the campers from the database
//        ArrayList<Camper> campers = CamperDAO.selectAll();
//        System.out.println("BJM-found "+campers.size()+" campers.  Going to welcome page");
//        model.addAttribute("campers", campers);
        //This will send the user to the welcome.html page.
        CamperEntity camper = new CamperEntity();
        model.addAttribute("camper", camper);

        return "camper/add";
    }

    @RequestMapping("/camper/edit")
    public String edit(Model model, HttpServletRequest request, HttpSession session) {

        boolean loggedIn = Utility.verifyLogin(session);
        //Verify that the user is logged in
        if (!loggedIn) {
            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
            model.addAttribute("user", new UserAccess());
            return "other/welcome";
        }

        CamperBO camperBO = new CamperBO(cr);
        info.hccis.camper.jpa.Camper camper = camperBO.getCamper(Integer.parseInt(request.getParameter("id")));

//        Camper camper = CamperDAO.select(Integer.parseInt(request.getParameter("id")));
        model.addAttribute("camper", camper);

        return "camper/add";
    }

    @RequestMapping("/camper/delete")
    public String delete(Model model, HttpServletRequest request, HttpSession session) {

        boolean loggedIn = Utility.verifyLogin(session);
        //Verify that the user is logged in
        if (!loggedIn) {
            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
            model.addAttribute("user", new UserAccess());
            return "other/welcome";
        }

        //This will send the user to the welcome.html page.
        CamperDAO.delete(Integer.parseInt(request.getParameter("id")));

        //reload camp type descriptions
        Iterable<info.hccis.camper.jpa.Camper> campers = cr.findAll();
        for (info.hccis.camper.jpa.Camper current : campers) {
            current.setCampTypeDescription(CodeValueDAO.getCodeValueDescription(new DatabaseConnection(), 2, current.getCampType()));
        }

        model.addAttribute("campers", campers);

        return "camper/list";
    }

    @RequestMapping("/camper/deleteall")
    public String deleteAll(Model model, HttpServletRequest request, HttpSession session) {

        boolean loggedIn = Utility.verifyLogin(session);
        //Verify that the user is logged in

        UserAccess ua = (UserAccess) session.getAttribute("loggedInUser");

        if (!loggedIn || ua.getUserTypeCode() != 2) {
            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
            if (ua.getUserTypeCode() != 2) {
                model.addAttribute("errorMessage", "Nice try!");
            }
            model.addAttribute("user", new UserAccess());
            return "other/welcome";
        }

        //This will send the user to the welcome.html page.
        cr.deleteAll();

        ArrayList<CamperEntity> campers = CamperDAO.selectAll();
        System.out.println("BJM-found " + campers.size() + " campers.  Going to welcome page");
        model.addAttribute("campers", campers);

        return "camper/list";
    }

    @RequestMapping("/camper/list")
    public String list(Model model, HttpServletRequest request, HttpSession session) {

        boolean loggedIn = Utility.verifyLogin(session);
        CamperBO camperBObj = new CamperBO();
        Integer cost;
        //Verify that the user is logged in
        if (!loggedIn) {
            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
            model.addAttribute("user", new UserAccess());
            return "other/welcome";
        }

        //ArrayList<Camper> campers = CamperDAO.selectAll();
        List<info.hccis.camper.jpa.Camper> campers = (List<info.hccis.camper.jpa.Camper>) cr.findAll();
        for (info.hccis.camper.jpa.Camper current : campers) {
            current.setCampTypeDescription(CodeValueDAO.getCodeValueDescription(new DatabaseConnection(), 2, current.getCampType()));

            cost = (int) camperBObj.getCost(current.getCampType());

            current.setCost(cost);
        }
        model.addAttribute("campers", campers);

        return "camper/list";
    }

    @RequestMapping("/camper/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("camper") CamperEntity camper, BindingResult result, HttpSession session) {

        boolean loggedIn = Utility.verifyLogin(session);
        //Verify that the user is logged in
        if (!loggedIn) {
            model.addAttribute("errorMessage", Utility.getMessage("login.not.logged.in"));
            model.addAttribute("user", new UserAccess());
            return "other/welcome";
        }

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            model.addAttribute("message", error);
            return "camper/add";
        }

        try {
            //CamperDAO.update(camper);
            info.hccis.camper.jpa.Camper jpaCamper = new info.hccis.camper.jpa.Camper();
            jpaCamper.setFirstName(camper.getFirstName());
            jpaCamper.setLastName(camper.getLastName());
            jpaCamper.setDob(camper.getDob());
            jpaCamper.setId(camper.getId());
            jpaCamper.setCampType(camper.getCampType());
            CamperBO camperBObj = new CamperBO();
            Integer cost;
            cost = (int) camperBObj.getCost(camper.getCampType());
            jpaCamper.setCost(cost);
            cr.save(jpaCamper);

        } catch (Exception ex) {
            Logger.getLogger(CamperController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Iterable<info.hccis.camper.jpa.Camper> campers = cr.findAll();
        for (info.hccis.camper.jpa.Camper current : campers) {
            current.setCampTypeDescription(CodeValueDAO.getCodeValueDescription(new DatabaseConnection(), 2, current.getCampType()));
        }
        model.addAttribute("campers", campers);

        return "camper/list";

    }
}
