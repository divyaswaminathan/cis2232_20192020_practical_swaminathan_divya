package info.hccis.camper.web;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.dao.CodeValueDAO;
import info.hccis.camper.dao.DatabaseConnection;
import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.jpa.UserAccess;
import info.hccis.camper.data.springdatajpa.UserAccessRepository;
import info.hccis.camper.entity.CamperEntity;
import info.hccis.camper.jpa.CodeValue;
import info.hccis.user.bo.UserBO;
import info.hccis.util.Utility;
import info.hccis.util.UtilityRest;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final UserAccessRepository ur;
    private final CamperRepository cr;

    /**
     * This constructor will inject the UserRepository object into this
     * controller. It will allow this class' methods to use methods from this
     * Spring JPA repository object.
     *
     * @since 20180524
     * @author BJM
     * @param ur
     */
    @Autowired
    public OtherController(UserAccessRepository ur, CamperRepository cr) {
        this.ur = ur;
        this.cr = cr;
    }

    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {

        UserAccess newOne = new UserAccess();
        model.addAttribute("user", newOne);
        session.setAttribute("loggedInUser", newOne);

        //Add the camp types to the session
        ArrayList<CodeValue> campTypes = CodeValueDAO.getCodeValues(new DatabaseConnection(), "2");
        session.setAttribute("campTypes", campTypes);

        //This will send the user to the welcome.html page.
        return "other/welcome";
    }

    @RequestMapping("/logout")
    public String logout(Model model, HttpSession session) {

        //Get the campers from the database
//        ArrayList<Camper> campers = CamperDAO.selectAll();
//        System.out.println("BJM-found "+campers.size()+" campers.  Going to welcome page");
        UserAccess newOne = new UserAccess();
        model.addAttribute("user", newOne);
        session.setAttribute("loggedInUser", newOne);

        //This will send the user to the welcome.html page.
        return "redirect:";
    }

    /**
     * This method will use the user object associated with the welcome page.
     *
     * @since 20180524
     * @author BJM
     *
     * @param model
     * @param user
     * @param session
     * @return
     */
    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") UserAccess user, HttpSession session) {

        session.removeAttribute("loggedInUser");

        long allUserCount = ur.count();
        System.out.println("BJM there are " + allUserCount + " users in the UserAccess table");

        if (!UserBO.authenticate(user, ur)) {

            //*******************************************
            //failed validation
            //*******************************************
            session.setAttribute("loggedInUser", user);
            model.addAttribute("errorMessage", Utility.getMessage("login.failed"));
            return "other/welcome";
        } else {

            //*******************************************
            //passed validation 
            //Get the campers from the database
            //send to the list page (camper/list)
            //*******************************************
            user = UserBO.getUserByUsername(user, ur);
            session.setAttribute("loggedInUser", user);

            model.addAttribute("message", "Welcome " + user.getUsername()+ " ("+UtilityRest.getWeatherNote()+")");

//            //Load the camper types from CodeValue for use when adding a camper
//            ArrayList<CodeValue> campTypes = CodeValueDAO.getCodeValues(new DatabaseConnection(), "2");
//            System.out.println("BJM - added " + campTypes.size() + " to the session for camp types");
//            session.setAttribute("CampTypes", campTypes);
//
//            //Give a message indicating that they have been logged out.
//            model.addAttribute("notificationMessage", Utility.getMessage("welcome", user.getFirstName()));
//            session.setAttribute("username", user.getUsername());
            //******************************************************************
            //Send the user to the landing page
            //******************************************************************
            //Switch to jpa....ArrayList<Camper> campers = CamperDAO.selectAll();
            Iterable<info.hccis.camper.jpa.Camper> campers = cr.findAll();
            for (info.hccis.camper.jpa.Camper current : campers) {
                current.setCampTypeDescription(CodeValueDAO.getCodeValueDescription(new DatabaseConnection(), 2, current.getCampType()));
            }
            model.addAttribute("campers", campers);

            return "camper/list";
        }
    }

}
