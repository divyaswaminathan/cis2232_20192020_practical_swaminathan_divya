package info.hccis.camper.soap;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.entity.CamperEntity;
import info.hccis.camper.jpa.Camper;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 * SOAP 
 * @author bjmaclean
 */
@WebService(serviceName = "CamperSoap")
public class CamperSoap {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "getCamper")
    public String hello(@WebParam(name = "camperId") String camperId) {
        CamperEntity camper = CamperDAO.select(Integer.parseInt(camperId));
        return camper.getFirstName()+" "+camper.getLastName();
    }
}
